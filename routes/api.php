<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('broadband/price', [
    'as'   => 'getBroadbandPrice',
    'uses' => 'PriceController@getBroadbandPrice',
]); // web api

//Route::get('broadband/price', '\App\Http\Controllers\PriceController@getBroadbandPrice');
Route::get('energy/price', '\App\Http\Controllers\PriceController@getEnergyPrice');
Route::patch('energy/price', '\App\Http\Controllers\PriceController@updateEnergyPrice');