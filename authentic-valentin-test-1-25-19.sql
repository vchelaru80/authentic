# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.22)
# Database: authentic-valentin-test
# Generation Time: 2019-01-25 20:52:04 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2019_01_25_150119_create_prices_table',1),
	(2,'2019_01_25_150120_create_product_variations_table',1),
	(3,'2019_01_25_150121_create_products_table',1),
	(4,'2019_01_25_150122_create_providers_table',1),
	(5,'2019_01_25_150124_create_types_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table prices
# ------------------------------------------------------------

DROP TABLE IF EXISTS `prices`;

CREATE TABLE `prices` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `product_variation_id` int(11) unsigned DEFAULT NULL,
  `amount` double(6,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_product_variation` (`product_variation_id`),
  CONSTRAINT `fk_prices_variations` FOREIGN KEY (`product_variation_id`) REFERENCES `product_variations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `prices` WRITE;
/*!40000 ALTER TABLE `prices` DISABLE KEYS */;

INSERT INTO `prices` (`id`, `updated_at`, `created_at`, `product_variation_id`, `amount`)
VALUES
	(1,'2019-01-25 20:29:57','2019-01-25 20:29:57',1,30.00),
	(2,'2019-01-25 20:29:57','2019-01-25 20:29:57',2,40.00),
	(3,'2019-01-25 20:29:57','2019-01-25 20:29:57',3,50.00),
	(4,'2019-01-25 20:29:57','2019-01-25 20:29:57',4,25.00),
	(5,'2019-01-25 20:29:57','2019-01-25 20:29:57',5,40.00),
	(6,'2019-01-25 20:29:57','2019-01-25 20:29:57',6,54.12),
	(7,'2019-01-25 20:29:57','2019-01-25 20:29:57',7,56.50),
	(8,'2019-01-25 20:29:57','2019-01-25 20:29:57',8,61.92),
	(9,'2019-01-25 20:29:57','2019-01-25 20:29:57',9,64.85),
	(10,'2019-01-25 20:29:57','2019-01-25 20:29:57',10,68.21),
	(11,'2019-01-25 20:29:57','2019-01-25 20:29:57',11,71.66),
	(12,'2019-01-25 20:29:57','2019-01-25 20:29:57',12,51.95),
	(13,'2019-01-25 20:29:57','2019-01-25 20:29:57',13,52.00),
	(14,'2019-01-25 20:29:57','2019-01-25 20:29:57',14,56.62),
	(15,'2019-01-25 20:29:57','2019-01-25 20:29:57',15,42.57),
	(16,'2019-01-25 20:29:57','2019-01-25 20:29:57',16,45.22),
	(17,'2019-01-25 20:29:57','2019-01-25 20:29:57',17,47.67);

/*!40000 ALTER TABLE `prices` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table product_variations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_variations`;

CREATE TABLE `product_variations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `product_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_product_variations_product_name` (`product_id`,`name`),
  KEY `idx_product_variations_product` (`product_id`),
  KEY `idx_product_variations_name` (`name`),
  CONSTRAINT `fk_product_variations_product` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `product_variations` WRITE;
/*!40000 ALTER TABLE `product_variations` DISABLE KEYS */;

INSERT INTO `product_variations` (`id`, `created_at`, `updated_at`, `product_id`, `name`)
VALUES
	(1,'2019-01-25 20:29:57','2019-01-25 20:29:57',1,NULL),
	(2,'2019-01-25 20:29:57','2019-01-25 20:29:57',2,NULL),
	(3,'2019-01-25 20:29:57','2019-01-25 20:29:57',3,NULL),
	(4,'2019-01-25 20:29:57','2019-01-25 20:29:57',4,NULL),
	(5,'2019-01-25 20:29:57','2019-01-25 20:29:57',5,NULL),
	(6,'2019-01-25 20:29:57','2019-01-25 20:29:57',6,'North'),
	(7,'2019-01-25 20:29:57','2019-01-25 20:29:57',6,'Mid'),
	(8,'2019-01-25 20:29:57','2019-01-25 20:29:57',6,'South'),
	(9,'2019-01-25 20:29:57','2019-01-25 20:29:57',7,'North'),
	(10,'2019-01-25 20:29:57','2019-01-25 20:29:57',7,'Mid'),
	(11,'2019-01-25 20:29:57','2019-01-25 20:29:57',7,'South'),
	(12,'2019-01-25 20:29:57','2019-01-25 20:29:57',8,'North'),
	(13,'2019-01-25 20:29:57','2019-01-25 20:29:57',8,'Mid'),
	(14,'2019-01-25 20:29:57','2019-01-25 20:29:57',8,'South'),
	(15,'2019-01-25 20:29:57','2019-01-25 20:29:57',9,'North'),
	(16,'2019-01-25 20:29:57','2019-01-25 20:29:57',9,'Mid'),
	(17,'2019-01-25 20:29:57','2019-01-25 20:29:57',9,'South');

/*!40000 ALTER TABLE `product_variations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `provider_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_products_provider_name` (`provider_id`,`name`),
  KEY `idx_products_provider` (`provider_id`),
  KEY `idx_products_name` (`name`),
  CONSTRAINT `fk_products_provider` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;

INSERT INTO `products` (`id`, `created_at`, `updated_at`, `provider_id`, `name`)
VALUES
	(1,'2019-01-25 20:29:57','2019-01-25 20:29:57',1,'100MB'),
	(2,'2019-01-25 20:29:57','2019-01-25 20:29:57',1,'200MB'),
	(3,'2019-01-25 20:29:57','2019-01-25 20:29:57',1,'300MB'),
	(4,'2019-01-25 20:29:57','2019-01-25 20:29:57',2,'17MB'),
	(5,'2019-01-25 20:29:57','2019-01-25 20:29:57',2,'72MB'),
	(6,'2019-01-25 20:29:57','2019-01-25 20:29:57',3,'Standard tariff'),
	(7,'2019-01-25 20:29:57','2019-01-25 20:29:57',3,'Green tariff'),
	(8,'2019-01-25 20:29:57','2019-01-25 20:29:57',4,'Standard tariff'),
	(9,'2019-01-25 20:29:57','2019-01-25 20:29:57',4,'Saver tariff');

/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table providers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `providers`;

CREATE TABLE `providers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_id` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_provider_name` (`name`),
  KEY `idx_provider_type` (`type_id`),
  CONSTRAINT `fk_providers_type` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `providers` WRITE;
/*!40000 ALTER TABLE `providers` DISABLE KEYS */;

INSERT INTO `providers` (`id`, `created_at`, `updated_at`, `name`, `type_id`)
VALUES
	(1,'2019-01-25 20:29:57','2019-01-25 20:29:57','BSNL',1),
	(2,'2019-01-25 20:29:57','2019-01-25 20:29:57','Airtel',1),
	(3,'2019-01-25 20:29:57','2019-01-25 20:29:57','Indane energy',2),
	(4,'2019-01-25 20:29:57','2019-01-25 20:29:57','Tata Power',2);

/*!40000 ALTER TABLE `providers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `types`;

CREATE TABLE `types` (
  `id` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `types` WRITE;
/*!40000 ALTER TABLE `types` DISABLE KEYS */;

INSERT INTO `types` (`id`, `created_at`, `updated_at`, `name`)
VALUES
	(1,'2019-01-25 20:29:57','2019-01-25 20:29:57','broadband'),
	(2,'2019-01-25 20:29:57','2019-01-25 20:29:57','energy');

/*!40000 ALTER TABLE `types` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
