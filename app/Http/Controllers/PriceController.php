<?php

namespace App\Http\Controllers;

use App\Price;
use App\Provider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PriceController extends Controller
{
    public function getBroadbandPrice(Request $request)
    {

        $input = $request->input();

        $validator = Validator::make($input, [
            'provider_name' => 'required|max:255',
            'product_name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            return response($errors, 400);
        }

        //$provider = (new Provider())->where('name', $input['provider_name'])->whereNull('')->with(['products.variant.price'])->get();
        //->join('user_profile', 'recommendations.posted_by', '=', 'user_profile.id')

        $prices = (new Provider())->select([
            'products.name as product_name',
            'prices.amount as price'
        ])->join('products', 'products.provider_id', '=', 'providers.id')
            ->join('product_variations', 'product_variations.product_id', '=', 'products.id')
            ->join('prices', 'prices.product_variation_id', '=', 'product_variations.id')
            ->where('providers.type_id', 1)
            ->where('providers.name', $input['provider_name'])
            ->where('products.name', $input['product_name'])
            ->get();

        return response($prices);
    }

    public function getEnergyPrice(Request $request)
    {
        $input = $request->input();

        $validator = Validator::make($input, [
            'provider_name' => 'required|max:255',
            'product_name' => 'required|max:255',
            'product_variant_name' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            return response($errors, 400);
        }

        $prices = (new Provider())->select([
            'products.name as product_name',
            'product_variations.name as product_variation_name',
            'prices.amount as price'
        ])->join('products', 'products.provider_id', '=', 'providers.id')
            ->join('product_variations', 'product_variations.product_id', '=', 'products.id')
            ->join('prices', 'prices.product_variation_id', '=', 'product_variations.id')
            ->where('providers.type_id', 2)
            ->where('providers.name', $input['provider_name'])
            ->where('products.name', $input['product_name'])
            ->where('product_variations.name', $input['product_variant_name'])
            ->get();

        return response($prices);
    }

    public function updateEnergyPrice(Request $request)
    {
        $input = $request->input();

        $validator = Validator::make($input, [
            'provider_name' => 'required|max:255|exists:providers,name',
            'product_name' => 'required|max:255|exists:products,name',
            'product_variant_name' => 'required|max:255|exists:product_variations,name',
            'price' => 'required|numeric',
        ]);

        $productVariation = (new Provider())->select([
            'products.id as product_id',
            'product_variations.id as product_variation_id'
        ])->join('products', 'products.provider_id', '=', 'providers.id')
            ->join('product_variations', 'product_variations.product_id', '=', 'products.id')
            ->where('providers.type_id', 2)
            ->where('providers.name', $input['provider_name'])
            ->where('products.name', $input['product_name'])
            ->where('product_variations.name', $input['product_variant_name'])
            ->first();


        if ($validator->fails()) {
            $errors = $validator->errors();

            return response($errors, 400);
        }

        if ($productVariation) {

            $success = (new Price())->where('product_variation_id',
                $productVariation->product_variation_id)->update(['amount' => $input['price']]);

            if ($success) {
                return response(['ok']);
            }
        }

        return response(['unable to save price'], 400);
    }
}
