<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariation extends Model
{
    protected $fillable = ['product_id', 'name'];

    protected $hidden = ['product_id'];

    public function price(){
        return $this->hasOne(Price::class);

    }
}
