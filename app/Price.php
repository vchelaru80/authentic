<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $fillable = ['product_variation_id', 'amount'];

    protected $hidden = ['product_variation_id'];

    protected $casts = [
        'amount' => 'float',
    ];
}
