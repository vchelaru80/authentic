<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['provider_id', 'name'];
    protected $hidden = ['provider_id'];

    public function variant(){
        return $this->hasOne(ProductVariation::class);
    }
}
