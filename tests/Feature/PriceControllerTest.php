<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class PriceControllerTest extends TestCase
{

    public static function setUpBeforeClass()
    {
        (new self())->createApplication();

    }

    function setUp()
    {
        parent::setUp();
    }

    public function testBroadband()
    {
        $response = $this->json('GET', '/api/broadband/price', ['provider_name' => 'BSNL', 'product_name' => '100mb']);

        $response->assertStatus(200);
        $response->assertJson([["product_name" => "100MB", "price" => 30]]);
    }

    public function testEnergy()
    {
        $response = $this->json('GET', '/api/energy/price', ['provider_name' => 'Indane energy', 'product_name' => 'Green tariff', 'product_variant_name' => 'Mid']);

        $response->assertStatus(200);
        $response->assertJson([["product_name" => "Green tariff", "product_variation_name" => "Mid", "price" => 68.21]]);


        $response = $this->json('GET', '/api/energy/price', ['provider_name' => '', 'product_name' => '', 'product_variant_name' => 'Mid']);

        $response->assertStatus(400);
    }
}