<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->unsignedInteger('provider_id')->nullable();
            $table->string('name', 255)->nullable();

            $table->unique(['provider_id', 'name'], 'idx_products_provider_name');
            $table->index('provider_id', 'idx_products_provider');
            $table->index('name', 'idx_products_name');

            //$table->foreign('provider_id', 'fk_products_provider')->references('id')->on('providers')->onDelete('CASCADE')->onUpdate('CASCADE');

        });

        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
