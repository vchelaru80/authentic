<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductVariationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_variations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->unsignedInteger('product_id')->nullable();
            $table->string('name', 255)->nullable();

            $table->unique(['product_id', 'name'], 'idx_product_variations_product_name');
            $table->index('product_id', 'idx_product_variations_product');
            $table->index('name', 'idx_product_variations_name');

            //$table->foreign('product_id', 'fk_product_variations_product')->references('id')->on('products')->onDelete('CASCADE')->onUpdate('CASCADE');

        });

        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_variations');
    }
}
