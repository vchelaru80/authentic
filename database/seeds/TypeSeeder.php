<?php

use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('types')->get()->count() == 0) {

            $data = <<<PRE
{
	"data":
	[
		{
			"id": 1,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"name": "broadband"
		},
		{
			"id": 2,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"name": "energy"
		}
	]
}
PRE;


            $data = json_decode($data, true);

            DB::table('types')->insert($data['data']);
        }
    }
}
