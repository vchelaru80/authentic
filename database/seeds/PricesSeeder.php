<?php

use Illuminate\Database\Seeder;

class PricesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // check if table users is empty
        if(DB::table('prices')->get()->count() == 0) {

            $data = <<<PRE
{
	"data":
	[
		{
			"id": 1,
			"updated_at": "2019-01-25 20:29:57",
			"created_at": "2019-01-25 20:29:57",
			"product_variation_id": 1,
			"amount": 30
		},
		{
			"id": 2,
			"updated_at": "2019-01-25 20:29:57",
			"created_at": "2019-01-25 20:29:57",
			"product_variation_id": 2,
			"amount": 40
		},
		{
			"id": 3,
			"updated_at": "2019-01-25 20:29:57",
			"created_at": "2019-01-25 20:29:57",
			"product_variation_id": 3,
			"amount": 50
		},
		{
			"id": 4,
			"updated_at": "2019-01-25 20:29:57",
			"created_at": "2019-01-25 20:29:57",
			"product_variation_id": 4,
			"amount": 25
		},
		{
			"id": 5,
			"updated_at": "2019-01-25 20:29:57",
			"created_at": "2019-01-25 20:29:57",
			"product_variation_id": 5,
			"amount": 40
		},
		{
			"id": 6,
			"updated_at": "2019-01-25 20:29:57",
			"created_at": "2019-01-25 20:29:57",
			"product_variation_id": 6,
			"amount": 54.12
		},
		{
			"id": 7,
			"updated_at": "2019-01-25 20:29:57",
			"created_at": "2019-01-25 20:29:57",
			"product_variation_id": 7,
			"amount": 56.5
		},
		{
			"id": 8,
			"updated_at": "2019-01-25 20:29:57",
			"created_at": "2019-01-25 20:29:57",
			"product_variation_id": 8,
			"amount": 61.92
		},
		{
			"id": 9,
			"updated_at": "2019-01-25 20:29:57",
			"created_at": "2019-01-25 20:29:57",
			"product_variation_id": 9,
			"amount": 64.85
		},
		{
			"id": 10,
			"updated_at": "2019-01-25 20:29:57",
			"created_at": "2019-01-25 20:29:57",
			"product_variation_id": 10,
			"amount": 68.21
		},
		{
			"id": 11,
			"updated_at": "2019-01-25 20:29:57",
			"created_at": "2019-01-25 20:29:57",
			"product_variation_id": 11,
			"amount": 71.66
		},
		{
			"id": 12,
			"updated_at": "2019-01-25 20:29:57",
			"created_at": "2019-01-25 20:29:57",
			"product_variation_id": 12,
			"amount": 51.95
		},
		{
			"id": 13,
			"updated_at": "2019-01-25 20:29:57",
			"created_at": "2019-01-25 20:29:57",
			"product_variation_id": 13,
			"amount": 52
		},
		{
			"id": 14,
			"updated_at": "2019-01-25 20:29:57",
			"created_at": "2019-01-25 20:29:57",
			"product_variation_id": 14,
			"amount": 56.62
		},
		{
			"id": 15,
			"updated_at": "2019-01-25 20:29:57",
			"created_at": "2019-01-25 20:29:57",
			"product_variation_id": 15,
			"amount": 42.57
		},
		{
			"id": 16,
			"updated_at": "2019-01-25 20:29:57",
			"created_at": "2019-01-25 20:29:57",
			"product_variation_id": 16,
			"amount": 45.22
		},
		{
			"id": 17,
			"updated_at": "2019-01-25 20:29:57",
			"created_at": "2019-01-25 20:29:57",
			"product_variation_id": 17,
			"amount": 47.67
		}
	]
}
PRE;
            $data = json_decode($data, true);

            DB::table('prices')->insert($data['data']);


            $data = <<<PRE
{
	"data":
	[
		{
			"id": 1,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"product_id": 1,
			"name": null
		},
		{
			"id": 2,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"product_id": 2,
			"name": null
		},
		{
			"id": 3,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"product_id": 3,
			"name": null
		},
		{
			"id": 4,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"product_id": 4,
			"name": null
		},
		{
			"id": 5,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"product_id": 5,
			"name": null
		},
		{
			"id": 6,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"product_id": 6,
			"name": "North"
		},
		{
			"id": 7,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"product_id": 6,
			"name": "Mid"
		},
		{
			"id": 8,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"product_id": 6,
			"name": "South"
		},
		{
			"id": 9,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"product_id": 7,
			"name": "North"
		},
		{
			"id": 10,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"product_id": 7,
			"name": "Mid"
		},
		{
			"id": 11,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"product_id": 7,
			"name": "South"
		},
		{
			"id": 12,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"product_id": 8,
			"name": "North"
		},
		{
			"id": 13,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"product_id": 8,
			"name": "Mid"
		},
		{
			"id": 14,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"product_id": 8,
			"name": "South"
		},
		{
			"id": 15,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"product_id": 9,
			"name": "North"
		},
		{
			"id": 16,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"product_id": 9,
			"name": "Mid"
		},
		{
			"id": 17,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"product_id": 9,
			"name": "South"
		}
	]
}
PRE;

            $data = json_decode($data, true);

            DB::table('product_variations')->insert($data['data']);

        }
    }
}
