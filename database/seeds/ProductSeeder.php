<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // check if table users is empty
        if(DB::table('products')->get()->count() == 0) {

            $data = <<<PRE
{
	"data":
	[
		{
			"id": 1,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"provider_id": 1,
			"name": "100MB"
		},
		{
			"id": 2,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"provider_id": 1,
			"name": "200MB"
		},
		{
			"id": 3,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"provider_id": 1,
			"name": "300MB"
		},
		{
			"id": 4,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"provider_id": 2,
			"name": "17MB"
		},
		{
			"id": 5,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"provider_id": 2,
			"name": "72MB"
		},
		{
			"id": 6,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"provider_id": 3,
			"name": "Standard tariff"
		},
		{
			"id": 7,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"provider_id": 3,
			"name": "Green tariff"
		},
		{
			"id": 8,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"provider_id": 4,
			"name": "Standard tariff"
		},
		{
			"id": 9,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"provider_id": 4,
			"name": "Saver tariff"
		}
	]
}
PRE;

            $data = json_decode($data, true);

            DB::table('products')->insert($data['data']);
        }

    }
}
