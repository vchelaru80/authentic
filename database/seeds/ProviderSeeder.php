<?php

use Illuminate\Database\Seeder;

class ProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('providers')->get()->count() == 0) {
            $data = <<<PRE
{
	"data":
	[
		{
			"id": 1,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"name": "BSNL",
			"type_id": 1
		},
		{
			"id": 2,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"name": "Airtel",
			"type_id": 1
		},
		{
			"id": 3,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"name": "Indane energy",
			"type_id": 2
		},
		{
			"id": 4,
			"created_at": "2019-01-25 20:29:57",
			"updated_at": "2019-01-25 20:29:57",
			"name": "Tata Power",
			"type_id": 2
		}
	]
}
PRE;

            $data = json_decode($data, true);

            DB::table('providers')->insert($data['data']);
        }
    }
}
